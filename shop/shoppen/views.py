from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import models


# Create your views here.
def index(request):
    items = models.Item.objects.all()
    return render(request, 'shoppen/index.html', context={'items': items})
def ansicht(request):
    items = models.Item.objects.all()
    return render(request, 'shoppen/ansicht.html', context={'items': items})

def speichern(request):
    if 'speichern' in request.POST:
        text = request.POST.get('text', '')
        menge = request.POST.get('menge', '')
        if text and menge:
            item = models.Item()
            item.text=text
            item.menge=menge
            item.save()
        if 'erledigt' in request.POST:
            print(request.POST.getlist('erledigt'))
            for item_id in request.POST.getlist('erledigt'):
                item= models.Item.objects.get(pk=item_id)
                item.delete()
    return redirect('index')


