from django.db import models
from django.http import HttpResponseRedirect

# Create your models here.
class Item(models.Model):
    text = models.TextField(blank=False, null=False, max_length=30)
    menge = models.TextField(blank=True, default="", max_length=10)

    def __str__(self):

        return "{} ({})".format(self.text, self.menge)




