from django.apps import AppConfig


class ShoppenConfig(AppConfig):
    name = 'shoppen'
