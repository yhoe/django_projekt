from django.shortcuts import render
from django.http import HttpResponse
from . import models

# Create your views here.
def index(request):
    context = {}
    context['todo'] = models.Tun.objects.filter(erledigt=False)
    context['erledigt'] = models.Tun.objects.filter(erledigt=True)
    return render(request, 'liste/index.html', context)

def add(request):
    if 'add' in request.POST:
        tun = models.Tun()
        tun.text = request.POST['text']
        tun.save()
    return redirect('index')

def delete(request):
    if 'id' in request.GET:
        tun = get_object_or_404(models.Tun, id=request.GET.get('id'))
        tun.delete()
    return redirect('index')

def refresh(request):
    if 'refresh' in request.POST:
        todo = models.Tun.objects.filter(erledigt=False)
        erledigt = models.Tun.objects.filter(erledigt=True)
        print("Neu erledigt: {}".format(request.POST.getlist('neu_erledigt')))
        print("Bleiben erledigt: {}".format(request.POST.getlist('bleiben_erledigt')))
        gerade_erledigt = []
        for tun in todo:
            print(str(tun.id))
            if str(tun.id) in request.POST.getlist('neu_erledigt'):
                print('Dos erledigt: {}'.format(tun.id))
                tun.erledigt=True
                gerade_erledigt.append(str(tun.id))
                tun.save()
        for tun in erledigt:
            if str(tun.id) not in request.POST.getlist('bleiben_erledigt') and str(tun.id) not in gerade_erledigt:
                print('Dos nicht mehr erledigt: {}'.format(tun.id))
                tun.erledigt=False
                tun.save()
        return redirect('index')
