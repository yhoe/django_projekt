from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from . import models
# Create your views here.

def index(request):
    spielers = models.Spieler.objects.all()
    return render(request, 'schiff/index.html', context={'spielers': spielers})

def ansicht(request):
    return render(request, 'schiff/ansicht.html', {})

def speichern(request):
    if 'speichern' in request.POST:
        text = request.POST.get('text', '')
        if text:
            spieler = models.Spieler()
            spieler.text=text
            spieler.save()
        if 'gelöscht' in request.POST:
            print(request.POST.getlist('gelöscht'))
            for spieler_id in request.POST.getlist('gelöscht'):
                spieler = models.Spieler.objects.get(pk = spieler_id)
                spieler.delete()
    return redirect('index')

'''def index(request):
	if request.method == 'POST':
		table = index(request.POST or None)

		if click in table ="Treffer":
			messages.success(request, ("Getroffen!"))
			return render(request, 'index.html', {'table': table})
            if click in table ="Daneben":
                message.success(request, ("Daneben")
                return render(request, 'index.html', {'table': table}'''
            #Beispiel für message
